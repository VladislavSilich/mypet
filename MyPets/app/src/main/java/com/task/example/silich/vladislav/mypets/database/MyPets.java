package com.task.example.silich.vladislav.mypets.database;

import android.net.Uri;

import io.realm.RealmObject;
import io.realm.annotations.Required;

/**
 * Created by Lenovo on 19.08.2017.
 */

public class MyPets extends RealmObject{
    @Required

    private String name;
    private String status;
    private Uri uriPet;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Uri getUriPet() {
        return uriPet;
    }

    public void setUriPet(Uri uriPet) {
        this.uriPet = uriPet;
    }
}
