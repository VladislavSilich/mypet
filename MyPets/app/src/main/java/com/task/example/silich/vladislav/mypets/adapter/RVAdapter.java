package com.task.example.silich.vladislav.mypets.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.task.example.silich.vladislav.mypets.R;
import com.task.example.silich.vladislav.mypets.network.responce.GetPetsResponce;

import java.util.List;

/**
 * Created by Lenovo on 18.08.2017.
 */

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.PersonViewHolder>{
    List<GetPetsResponce> responces;
    Context context;

    public RVAdapter(Context context, List<GetPetsResponce> persons){
        this.context = context;
        responces = persons;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_layout, parent, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {
        String a1 = "string";
        holder.txtStatus.setText(responces.get(position).getStatus());
        holder.txtName.setText(responces.get(position).getName());
        holder.txtCategory.setText(String.valueOf(responces.get(position).getCategory().getName()));
        if (responces.get(position).getPhotoUrls() != null) {
            if (responces.get(position).getPhotoUrls().get(0).equals(a1) || responces.get(position).getPhotoUrls().get(0) == null
                    ) {
                holder.photoPets.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.image));
            }
            else {
                Glide.with(context).load(responces.get(position).getPhotoUrls().get(0)).load(String.valueOf(holder.photoPets));
            }
        }
    }

    @Override
    public int getItemCount() {
        return responces.size();
    }

    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView txtName;
        TextView txtStatus;
        TextView txtCategory;
        ImageView photoPets;
        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            txtName = (TextView)itemView.findViewById(R.id.txt_name);
            txtStatus = (TextView)itemView.findViewById(R.id.txt_status);
            txtCategory = (TextView)itemView.findViewById(R.id.txt_category);
            photoPets = (ImageView)itemView.findViewById(R.id.photoPets);
        }
    }

}
