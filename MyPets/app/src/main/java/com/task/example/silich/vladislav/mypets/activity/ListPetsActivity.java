package com.task.example.silich.vladislav.mypets.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.task.example.silich.vladislav.mypets.R;
import com.task.example.silich.vladislav.mypets.adapter.RVAdapter;
import com.task.example.silich.vladislav.mypets.manager.DataManager;
import com.task.example.silich.vladislav.mypets.network.responce.GetPetsResponce;
import com.task.example.silich.vladislav.mypets.utils.NetworkStatusChecker;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListPetsActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    DataManager mDataManager;
    List<String> statusList;
    List<GetPetsResponce> responceList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListPetsActivity.this,AddPetsActivity.class);
                startActivity(intent);

            }
        });


        responceList = new ArrayList<>();
        statusList = new ArrayList<>();
        statusList.add("available");
        statusList.add("pending");
        statusList.add("sold");
        mDataManager = DataManager.getInstance();
        getPets();


        }

    private void getPets() {
        if (NetworkStatusChecker.isNetworkAvailible(this) == true) {
            Call<List<GetPetsResponce>> call = mDataManager.getAllPets(statusList);
            call.enqueue(new Callback<List<GetPetsResponce>>() {
                @Override
                public void onResponse(Call<List<GetPetsResponce>> call, Response<List<GetPetsResponce>> response) {
                    responceList.addAll(response.body());
                    RVAdapter adapter = new RVAdapter(ListPetsActivity.this, responceList);
                    recyclerView.setAdapter(adapter);
                }

                @Override
                public void onFailure(Call<List<GetPetsResponce>> call, Throwable t) {

                }
            });

            }
        else {
            Toast.makeText(this, "Network not available", Toast.LENGTH_LONG).show();
        }
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
