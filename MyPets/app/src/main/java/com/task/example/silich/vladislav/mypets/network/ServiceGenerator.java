package com.task.example.silich.vladislav.mypets.network;

import com.task.example.silich.vladislav.mypets.utils.ConstantManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Lenovo on 03.05.2017.
 */

public class ServiceGenerator {
    private static String BASE_URL = ConstantManager.BASE_URL;
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder sBuilder = new Retrofit.Builder()
            .baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);

        Retrofit retrofit = sBuilder.client(httpClient.build())
                .build();
        return retrofit.create(serviceClass);
    }
}