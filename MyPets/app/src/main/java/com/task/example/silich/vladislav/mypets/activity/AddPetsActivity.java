package com.task.example.silich.vladislav.mypets.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.Toast;

import com.task.example.silich.vladislav.mypets.R;
import com.task.example.silich.vladislav.mypets.database.MyPets;
import com.task.example.silich.vladislav.mypets.manager.DataManager;
import com.task.example.silich.vladislav.mypets.network.request.AddPetsRequest;
import com.task.example.silich.vladislav.mypets.network.responce.AddPetsResponce;
import com.task.example.silich.vladislav.mypets.network.responce.UploadImageResponce;
import com.task.example.silich.vladislav.mypets.utils.ConstantManager;
import com.task.example.silich.vladislav.mypets.utils.NetworkStatusChecker;

import java.io.File;

import io.realm.Realm;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPetsActivity extends AppCompatActivity implements View.OnClickListener{
    EditText edtName,edtStatus,edtIdUpdate,edtNameUpdate,edtStatusUpdate;
    DataManager dataManager;
    Button btnAdd,btnUpdate;
    AddPetsRequest petsRequest;
    TabHost tabHost;
    private Realm mRealm;
    long idPet;
    private Uri  mSelectedImage;
    String name;
    String status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pets);
        mRealm = Realm.getInstance(this);
        dataManager = DataManager.getInstance();
        edtName = (EditText)findViewById(R.id.edtName);
        edtStatus = (EditText)findViewById(R.id.edtStatus);
        edtNameUpdate = (EditText)findViewById(R.id.edtNameUpdate);
        edtStatusUpdate = (EditText)findViewById(R.id.edtStatusUpdate);
        edtIdUpdate = (EditText)findViewById(R.id.edtIdUpdate);
        btnAdd = (Button)findViewById(R.id.btnAdd);
         petsRequest = new AddPetsRequest();

        btnAdd.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);

        tabHost = (TabHost)findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("tag1");
        tabSpec.setContent(R.id.linearLayoutAdd);
        tabSpec.setIndicator("Add Pet");
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("tag2");
        tabSpec.setContent(R.id.linearLayoutUpdate);
        tabSpec.setIndicator("Update Pet");
        tabHost.addTab(tabSpec);


        tabHost.setCurrentTab(0);
    }

    private void addPets() {
        name = String.valueOf(edtStatus.getText().toString());
        status = String.valueOf(edtName.getText().toString());
        petsRequest.setStatus(name);
        petsRequest.setName(status);
        if (NetworkStatusChecker.isNetworkAvailible(this) == true) {
            Call<AddPetsResponce> call = dataManager.addPets(petsRequest);
            call.enqueue(new Callback<AddPetsResponce>() {
                @Override
                public void onResponse(Call<AddPetsResponce> call, Response<AddPetsResponce> response) {
                    idPet = response.body().getId();
                    openDialog(idPet);
                }

                @Override
                public void onFailure(Call<AddPetsResponce> call, Throwable t) {

                }
            });
        }else {
            Toast.makeText(this,"Network not available",Toast.LENGTH_LONG).show();
        }
    }

        public void openDialog(final long idPet){
        final AlertDialog.Builder builder = new AlertDialog.Builder(AddPetsActivity.this);
        builder.setTitle("Message")
                .setMessage(
                        "Do you want to add an image?")
                .setCancelable(false)
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    loadPhotoGallery();
                    Toast.makeText(AddPetsActivity.this,String.valueOf(idPet),Toast.LENGTH_LONG).show();
                }
            });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private void loadPhotoGallery(){
        Intent takeGalleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        takeGalleryIntent.setType("image/*");
        startActivityForResult(Intent.createChooser(takeGalleryIntent,"Выберите фото"), ConstantManager.REQUEST_GALLERY_PICTURES);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case ConstantManager.REQUEST_GALLERY_PICTURES:
                if (resultCode == RESULT_OK && data != null){
                    mSelectedImage = data.getData();
                    uploadImage(mSelectedImage);
                    mRealm.beginTransaction();
                    MyPets myPets = mRealm.createObject(MyPets.class);
                    myPets.setName(name);
                    myPets.setStatus(status);
                    myPets.setUriPet(mSelectedImage);
                    mRealm.commitTransaction();
                    Intent intent = new Intent(AddPetsActivity.this,ListPetsActivity.class);
                    startActivity(intent);
                }
                break;
        }
    }

    private void uploadImage(Uri mSelectedImage) {
        File file = new File(mSelectedImage.getPath());
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
        if (NetworkStatusChecker.isNetworkAvailible(this) == true) {
            Call<UploadImageResponce> call = dataManager.uploadImagePet(idPet, filePart);
            call.enqueue(new Callback<UploadImageResponce>() {
                @Override
                public void onResponse(Call<UploadImageResponce> call, Response<UploadImageResponce> response) {
                    response.body();
                }

                @Override
                public void onFailure(Call<UploadImageResponce> call, Throwable t) {

                }
            });
        } else {
            Toast.makeText(this, "Network not available", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case  R.id.btnAdd:
                addPets();
                break;
            case R.id.btnUpdate:
                updatePets();
                break;
        }
    }

    private void updatePets() {
        AddPetsRequest updatePets = new AddPetsRequest();
        String updateName = edtNameUpdate.getText().toString();
        String updateStatus = edtStatusUpdate.getText().toString();
        String updateId = edtIdUpdate.getText().toString();
        if (!updateId.isEmpty()){
            updatePets.setName(updateName);
            updatePets.setStatus(updateStatus);
            if (NetworkStatusChecker.isNetworkAvailible(this) == true) {
                Call<AddPetsResponce> call = dataManager.updatePets(updatePets);
                call.enqueue(new Callback<AddPetsResponce>() {
                    @Override
                    public void onResponse(Call<AddPetsResponce> call, Response<AddPetsResponce> response) {
                        if (response.code() == 200) {
                            Toast.makeText(AddPetsActivity.this, "Done!", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddPetsResponce> call, Throwable t) {

                    }
                });
            }else {
                Toast.makeText(this,"Network not available",Toast.LENGTH_LONG).show();
            }
        }
        else{
            Toast.makeText(AddPetsActivity.this,"Please, enter id",Toast.LENGTH_LONG).show();
        }
    }
    @Override

    public void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }
}

