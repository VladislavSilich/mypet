package com.task.example.silich.vladislav.mypets.network;

import com.task.example.silich.vladislav.mypets.network.request.AddPetsRequest;
import com.task.example.silich.vladislav.mypets.network.responce.AddPetsResponce;
import com.task.example.silich.vladislav.mypets.network.responce.GetPetsResponce;
import com.task.example.silich.vladislav.mypets.network.responce.UploadImageResponce;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Lenovo on 18.08.2017.
 */

public interface RestService {
    @GET("pet/findByStatus")
    Call<List<GetPetsResponce>> getPets (@Query("status") List<String> arrayStatus);

    @POST("pet")
    Call<AddPetsResponce> addPet(@Body AddPetsRequest addPetsReq);

    @Multipart
    @POST("pet/{petId}/uploadImage")
        Call<UploadImageResponce> uploadPetImage(@Path("petId") long id, @Part MultipartBody.Part filePart);
    @PUT("pet")
    Call<AddPetsResponce> updatePet (@Body AddPetsRequest updatePetsReq);
}
