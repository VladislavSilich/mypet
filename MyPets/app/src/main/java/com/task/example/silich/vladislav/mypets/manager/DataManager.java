package com.task.example.silich.vladislav.mypets.manager;

import com.task.example.silich.vladislav.mypets.network.RestService;
import com.task.example.silich.vladislav.mypets.network.ServiceGenerator;
import com.task.example.silich.vladislav.mypets.network.request.AddPetsRequest;
import com.task.example.silich.vladislav.mypets.network.responce.AddPetsResponce;
import com.task.example.silich.vladislav.mypets.network.responce.GetPetsResponce;
import com.task.example.silich.vladislav.mypets.network.responce.UploadImageResponce;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;

/**
 * Created by Lenovo on 18.08.2017.
 */

public class DataManager {

    private static DataManager INSTANCE = null;
    private RestService restService;

    private DataManager(){
        this.restService = ServiceGenerator.createService(RestService.class);
    }

    public static DataManager getInstance(){
        if (INSTANCE == null){
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    public Call<List<GetPetsResponce>> getAllPets (List<String> status){
        return restService.getPets(status);
    }

    public Call<AddPetsResponce> addPets(AddPetsRequest addPetsRequest){
        return restService.addPet(addPetsRequest);
    }

    public Call<UploadImageResponce> uploadImagePet(long id, MultipartBody.Part file){
        return restService.uploadPetImage(id,file);
    }
    public Call<AddPetsResponce> updatePets(AddPetsRequest updatePetsReq){
        return restService.updatePet(updatePetsReq);
    }
}
